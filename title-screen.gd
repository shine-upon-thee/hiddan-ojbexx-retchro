extends Control

func _ready():
	get_node("sound").play("titlescreen")
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _on_startgame_pressed():
	get_node("sound").play("startgame")
	get_node("title").hide()
	get_node("detective").hide()
	get_node("magglass").hide()
	get_node("intro").show()
	get_node("timer").start()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	pass

func _on_timer_timeout():
	get_node("/root/global").goto_scene("story")
	pass
