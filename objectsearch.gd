
extends Control

var max_objects = 12
var min_objects = 6

var object_count

var object_names = [] # we read in the filenames

func _ready():
	randomize()
	random_background()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	set_process_input(true)
	read_object_files()
	generate_objects()
	# hide overlay
	get_node("overlay").set_layer(-1)
	# cache viewport to image
	get_tree().get_root().queue_screen_capture()
	# show overlay
	get_node("overlay").set_layer(1)
	#get_node("timer").start()
	pass

func _input(ev):
	if (ev.type == InputEvent.MOUSE_MOTION):
		var tex = ImageTexture.new()
		var cap = get_tree().get_root().get_screen_capture()
		tex.set_data(cap)
		get_node("overlay/colormap").set_texture(tex)

		var color = get_node("overlay/colormap").get_texture().get_data().get_pixel(ev.pos.x, ev.pos.y) 
		#get_tree().get_root().get_screen_capture().get_pixel(ev.pos.x, ev.pos.y)
		print(color)
		get_node("overlay/cursor").set_pos(ev.pos)

func object_found(obj):
	# play a random sound
	get_node("sound").play("twiddly" + str(1 + (randi() % 8)))
	object_count -= 1
	# delete the object
	get_node(obj).queue_free()
	# grey out the object label
	get_node("overlay/objlist/"+obj+"label").add_color_override("font_color", Color(0.2, 0.2, 0.2))
	# did the player find all the objects?
	if object_count < 1:
		#TODO not sure if these lines will actually help with the music bug...
		#get_node("music").stop()
		#get_node("music").
		#remove_and_delete_child(get_node("music"))
		get_node("overlay/instructions").hide()
		get_node("overlay/win").show()
		get_node("sound").play("win")
		get_node("timer").start()
	pass # replace with function body


func _on_timer_timeout():
	if !get_node("overlay/instructions").is_hidden():
		get_node("overlay/instructions").hide()
	else:
		get_node("/root/global").goto_scene("story")
	pass # replace with function body


func generate_objects():
	object_count = min_objects + (randi() % (max_objects + 1 - min_objects))
	for i in range(object_count):
		# choose a random object from the files array
		var filename = object_names[ randi() % object_names.size() ]
		var obj = Sprite.new()
		var tex = load("res://objs/" + filename + ".png")
		obj.set_texture(tex)
		obj.set_centered(false)
		obj.set_name("object"+str(i))
		# set rotation... let's only rotate objects some of the time
		if randf() > 0.4:
			# restrict rotation possibilities to 5 steps from -90 to +90
			var deg = ((randi() % 5) * 45) - 90
			obj.set_rot( deg2rad(deg) )
		# set horizontal flip
		obj.set_flip_h( randi() % 2 )
		# set scale
		var scale = 2 + randi() % 3
		obj.set_scale(Vector2(scale, scale))
		# set position
		#TODO this should maybe make the objects not overlap?
		#TODO should determine positioning based on screen res (1024x576) minus sprite width
		var pos = Vector2( 10 + randi() % 974 , 10 + randi() % 516)
		obj.set_pos(pos)
		# create button
		var btn = TextureButton.new()
		btn.set_name("btn")
		# set it to stretch with parent node
		btn.set_anchor_and_margin(MARGIN_RIGHT, ANCHOR_END, 0)
		btn.set_anchor_and_margin(MARGIN_BOTTOM, ANCHOR_END, 0)
		# build and set clickmask from alpha
		var cmask = BitMap.new()
		cmask.create_from_image_alpha( tex.get_data() )
		btn.set_click_mask(cmask)
		# connect the pressed signal
		btn.connect("pressed",self,"object_found",["object"+str(i)])
		# add button to sprite, and sprite to root node
		obj.add_child(btn)
		add_child(obj)
		var label = Label.new()
		var font = load("res://pixellated.fnt")
		label.add_font_override("font",font)
		label.set_pos(Vector2(10,20 * i))
		label.set_text(filename)
		label.set_name(obj.get_name() + "label")
		# FIXME		label.add_color_override("custom_colors/font_color_shadow",Color(0,0,0))
		# FIXME		label.add_constant_override("custom_constants/shadow_offset_x", 1)
		get_node("overlay/objlist").add_child(label)
	pass

func read_object_files():
# this function reads our object directory for *.png files and sticks them in an array
	var dir = Directory.new()
	dir.open("res://objs/")
	dir.list_dir_begin()
	while 1:
		var file = dir.get_next()
		if file == "":
			break
		if file.extension() == "png":
			object_names.append(file.basename())
	dir.list_dir_end()
	pass

func random_background():
# this function reads our background directory for *.png files and chooses one randomly
	var backgrounds = []
	var dir = Directory.new()
	dir.open("res://bgs/")
	dir.list_dir_begin()
	while 1:
		var file = dir.get_next()
		if file == "":
			break
		if file.extension() == "png":
			backgrounds.append(file)
	dir.list_dir_end()
	get_node("background").set_texture( load("res://bgs/" + backgrounds[ randi() % backgrounds.size() ]) )
	pass
