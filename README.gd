Hiddan Ojbexx Retchro
---------------------
http://www.shineuponthee.com/


We poured over research and compiled statistics for months and came to the conclusion:

Gamers love games.

They love certain aspects more than others, though, and we decided to make a game that
combines all of the elements into one ultimate video game.

It's clear from the flood of pixel-art blockbuster hits that gamers are fed up with
games that are easy on the eyes. The fact that a game like Thimbleweed Park can be
pulled out of a clogged toilet to the tune of a half a million proves this. Meanwhile,
gamers turn up their noses and close their wallets to games like Dog Mendonca, which
just barely squeaked by their $30K goal, and The Black Glove, which looked gorgeous
but failed funding because who wants a unique game that looks pretty?

Steam is overloaded with hidden object games from companies like Artifex Mundi, and
gamers eat it up like it's pizza. So obviously the routine gameplay of finding mundane
items in a static background is exciting! Scavenger hunts are the new guns. It's
almost 2015!

Guitar riffs! Who doesn't like guitar riffs? We put those in here, too.

We also loaded it up with character-driven story, because that's the way to do it. You
get to write the story! And speaking of the story, it has lots of typos because other
big games, like Shadowrun Returns has them, and it just adds to the immersion. You can
almost feel the education system failing when you play Hiddan Ojbexx Retchro!

This is the ultimate video game.

Now licensed under the MIT license.

