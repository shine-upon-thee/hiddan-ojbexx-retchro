
extends Control

var chars_typed = 0

var max_chapter = 5 # how many story chapters to play before ending the game TODO - randomize this?
var min_chapters = 3 # TODO unused at the moment

var chapter

func _ready():
	chapter = get_node("/root/global").story_chapter
	print("chapter " + str(chapter))
	#chapter = max_chapter + 1
	if (chapter > max_chapter):
		get_node("text/text").set_text("STORY_END")
	elif (chapter > 0):
		get_node("text/text").set_text("STORY_"+str(chapter))
	get_node("/root/global").story_chapter += 1
		
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	set_process_input(true)
	#get_node("timer").start() # autostart is not working... opened a bug report
	pass


func _input(ev):
	if (ev.type == InputEvent.MOUSE_MOTION):
		get_node("cursor").set_pos(ev.pos)
	elif (ev.type == InputEvent.KEY and !ev.is_echo() and ev.pressed):
		get_node("gibberish").stop()
		get_node("timer").stop()
		# I'm unsure why I need to subtract 2 from the total character count... opened a bug report
		if ( chars_typed < (get_node("text/text").get_total_character_count() - 2 ) ):
			get_node("timer").start()
			chars_typed += 1
			#print (chars_typed)
			#print (get_node("text/text").get_total_character_count())
			get_node("sound").play("type")
			get_node("text/text").set_visible_characters( chars_typed )
		elif chapter > max_chapter:
			get_node("/root/global").story_chapter = 0
			# fade out and automatically go back to the title screen
			if !get_node("anim").is_playing():
				get_node("anim").play("fade")
		elif get_node("next-scene").is_disabled():
			get_node("cursor").show()
			get_node("next-scene").set_disabled(false)


func _on_nextscene_pressed():
	#get_node("gibberish").free()
	#get_node("gibberish").set_stream(null)
	#var stream = get_node("gibberish").get_stream()
	#var rid = stream.get_rid()
	#rid.queue_free()
	#remove_and_delete_child(get_node("gibberish"))
	if chapter > max_chapter:
		get_node("/root/global").goto_scene("title-screen")
	else:
		get_node("/root/global").goto_scene("objectsearch")
	pass # replace with function body

